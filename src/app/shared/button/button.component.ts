import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-button',
  standalone: true,
  imports: [],
  templateUrl: './button.component.html',
  styleUrl: './button.component.css'
})
export class ButtonComponent {
  @Input() buttonText: string = 'Obtenir Signal'; // Création du props bouton avec "@Input"
  @Input() buttonStyle: string = '';
}
